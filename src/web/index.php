<?php

include_once(__DIR__ . '\\..\\script.php');

/**
 * Loads the chef runtime settings.
 */
function loadChefSettings() {
  // Look for the file a couple of levels up...
  $depth = 0;
  $contentsFile = NULL;
  while ($contentsFile == NULL && $depth < 3) {
    $prefix = '';
    for ($x = 0; $x < $depth; $x++) {
      $prefix .= '../';
    }
    if ($contentsFile = @file_get_contents($prefix . 'chef-runtime.path')) {
      break;
    }
    $depth++;
  }
  if ($settingsFile = @file_get_contents($contentsFile . '\\chef-settings.json')) {
    $_SERVER['CHEF_SETTINGS'] = json_decode($settingsFile);
    if ($_SERVER['CHEF_SETTINGS'] == NULL
      && json_last_error() !== \JSON_ERROR_NONE) {
      throw new \Exception("Error parsing chef settings file: " . json_last_error_msg());
    }
  }
}

loadChefSettings();

echo isset($_SERVER['CHEF_SETTINGS']) ? json_encode($_SERVER['CHEF_SETTINGS'], JSON_PRETTY_PRINT) : 'DEPLOYMENT NOT FOUND';